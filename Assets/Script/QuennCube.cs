using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuennCube : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")||other.name== "Player")
        {
            RollingBall rollingBall = other.GetComponent<RollingBall>();
            rollingBall.queenCubeEaten++;
            rollingBall.CheckThreshold();

            //other.GetComponent<RollingBall>().queenCubeEaten++;
            //other.GetComponent<RollingBall>().CheckThreshold();
            Destroy(gameObject);
        }
    }

}
