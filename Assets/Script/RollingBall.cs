using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RollingBall : MonoBehaviour
{
    public Text progressUI;
    public TextMeshProUGUI progressText;

    public GameObject gate;

    public int queenCubeThreshold = 5;
    public int queenCubeEaten = 0;
    
    public float speed = 3.0f;
    public Rigidbody rigid;
    public float horizontal;
    public float vertical;


    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        progressUI.text = $"Queen Cube Eaten: 0\r\nQueen Cube Remaining: {queenCubeThreshold}";
        
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        
        Vector3 direction = new Vector3(horizontal, 0.0f, vertical);

        rigid.AddForce(direction * speed);
    }

    public void CheckThreshold()
    {
        progressUI.text = $"Queen Cube Eaten: {queenCubeEaten}\r\nQueen Cube Remaining: {queenCubeThreshold - queenCubeEaten}";

        if (queenCubeEaten >= queenCubeThreshold) 
        {
            gate.SetActive(false);
        }
    }
}
